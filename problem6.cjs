function Problem6(inventory){
    let car_make_array = []
    for (index = 0; index < inventory.length; index++){
        let element = inventory[index].car_make;
        if(element == "BMW" || element == "Audi"){
            car_make_array.push(inventory[index])
        }
    }
    let output = JSON.stringify(car_make_array)
    return output
}

module.exports = Problem6;