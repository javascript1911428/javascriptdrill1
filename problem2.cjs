function Problem2(inventory){
    let last_car = inventory[0];
    let last_car_year = last_car.car_year;
    for(let index = 0; index < inventory.length; index++){
        if(last_car_year < inventory[index].car_year){
            last_car_year = inventory[index].car_year;
            last_car = inventory[index];
        }
    }

    return last_car
}

module.exports = Problem2;