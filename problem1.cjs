function Problem1(inventory,id0) {
    if (typeof(inventory) == "object" && inventory != null && id0 != null) {
        if (!Array.isArray(inventory)) {
            return [];
        }
        for (let index = 0; index < inventory.length; index++){
            if(inventory[index].id == id0){
                return inventory[index];
            }
        }
    }

    return [];
}
module.exports = Problem1;
